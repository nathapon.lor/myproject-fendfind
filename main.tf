provider "aws" {
  region = "ap-southeast-1"
  access_key = "AKIA6IRP3DJXXL7NKVGP"
  secret_key = "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"
}


#Create Database Security Group

resource "aws_security_group" "fend_allow_db" {
  name        = "allow_db"
  description = "Allow db inbound traffic"
  vpc_id      = "vpc-0d7462197f0a1e542"

  ingress {
    description      = "db from VPC"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "fend-allow_db"
  }
}

#Create WP Security Group

resource "aws_security_group" "sg-fendmywp" {
  name        = "WordPress-sg"
  description = "Allow SSH and HTTP"
  vpc_id      = "vpc-0d7462197f0a1e542"

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

   ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "sg-fendmywp"
  }
}

#Create WordPress OS on Ubuntu

resource "aws_instance" "WordPress-OS" {
  ami = "ami-018c1c51c7a13e363"
  instance_type = "t2.micro"
  key_name = "poc-fendfind-key"
  associate_public_ip_address = false
  subnet_id = "subnet-0bd4a2e7ae6681b2c"
  vpc_security_group_ids = [aws_security_group.sg-fendmywp.id]
  availability_zone = "ap-southeast-1a"
  user_data = file("script.sh")

  tags = {
    "Name" = "WordPress-OS"
  }
}

#Create MySQL Database
resource "aws_db_instance" "wordpressfenddb1" {
  allocated_storage = 20
  max_allocated_storage = 40
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "5.7"
  instance_class = "db.t2.micro"
  name = "mydb"
  username = "admin"
  password = "Liverpoolth123"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot = true
  vpc_security_group_ids = [aws_security_group.fend_allow_db.id]
  publicly_accessible = false
  port = 3306
}

#Create ELB

resource "aws_elb" "bar" {
  name               = "poc-fend-elb-terraform"
  availability_zones = ["ap-southeast-1a"]
 
  

  
  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

   health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8000/"
    interval            = 30
  }


  tags = {
    Name = "poc-fend-elb-terraform"
  }


